const usageString =
`Usage: deno run --unstable --allow-net scan.ts [OPTIONS]
Options:
  --hostname <hostname>, default 'localhost'.
  --port <port>, default '3306'.
  --username <username>, default 'root'.
  --password <password>, default ''.
  --database <database>, default 'tv3'.
  --start <start>|min|max, default '0'.
    * 'min' gets replaced with the smallest video ID stored in the database.
    * 'max' gets replaced with the greatest video ID stored in the database.
  --end <end>|min|max, default 'Infinity'
    * 'min' and 'max' behave as in '--start'.
    * If 'start' > 'end', the scan advances backwards.
  --help
    * Display this help and exit.`;

import * as Std from 'https://deno.land/std@0.110.0/flags/mod.ts';
import { Client as MySQLClient } from 'https://deno.land/x/mysql@v2.9.0/mod.ts';
import * as XML from 'https://deno.land/x/xml@v1.0.2/mod.ts';
import * as SQLBuilder from 'https://deno.land/x/sql_builder@v1.8.0/mod.ts';

const Consts = {
    videosPerSecond: 60,
    saveEveryMs: 30000,
    maxErrors: 3,
    tv3Encoding: 'ISO-8859-1',
};

const DefaultArgs = {
    hostname: 'localhost',
    port: 3306,
    username: 'root',
    password: '',
    database: 'tv3',
    start: 0,
    end: Infinity,
};

const ProgArgs = Std.parse(Deno.args, {
    string: ['hostname', 'username', 'password', 'database'],
    default: DefaultArgs,
});

const validateArgs = () => {
    const invalidArg = (arg: string) => {
        throw Error(`Invalid value for option '${arg}': ${ProgArgs[arg]}`);
    };
    for (const arg of ['hostname', 'username', 'password', 'database'])
        if (typeof ProgArgs[arg] != 'string')
            invalidArg(arg);
    if (!Number.isInteger(ProgArgs.port))
        invalidArg('port');
    for (const arg of ['start', 'end'])
        if ( !Number.isInteger(ProgArgs[arg]) &&
             ProgArgs[arg] != Infinity &&
             (typeof ProgArgs[arg] != 'string' ||
              !['MIN', 'MAX'].includes(ProgArgs[arg].toUpperCase())) )
            invalidArg(arg);
};

interface TableSpec {
    readonly table: string,
    readonly keys: string[],
    readonly nonUniqueKeys: string[],
};

type VideoResult = {
    readonly info: Info,
    readonly videos: Video[],
    readonly subtitles: Subtitle[],
};

class Info {
    constructor(
        public date: Date,
        public videoId: string,
        public channel: string | null,
        public seriesId: string | null,
        public seriesName: string | null,
        public episode: number | null,
        public title: string | null,
        public duration: string | null,
        public broadcastDate: Date | null,
        public expirationDate: Date | null,
    ) {
    }
    static readonly table = "info";
    static readonly keys = [
        'date', 'videoId', 'channel', 'seriesId', 'seriesName', 'episode',
        'title', 'duration', 'broadcastDate', 'expirationDate',
    ];
    static readonly nonUniqueKeys = [
        'date', 'channel', 'seriesId', 'seriesName', 'episode', 'title',
        'duration', 'broadcastDate', 'expirationDate',
    ];
};

class Video {
    constructor(
        public date: Date,
        public videoId: string,
        public label: string | null,
        public format: string | null,
        public url: string | null,
    ) {
    }
    static readonly table = "videos";
    static readonly keys = ['date', 'videoId', 'label', 'format', 'url'];
    static readonly nonUniqueKeys = ['date'];
};

class Subtitle {
    constructor(
        public date: Date,
        public videoId: string,
        public language: string | null,
        public format: string | null,
        public url: string | null,
    ) {
    }
    static readonly table = "subtitles";
    static readonly keys = ['date', 'videoId', 'language', 'format', 'url'];
    static readonly nonUniqueKeys = ['date'];
};

const log = function(...args: any[]) {
    console.log(`${new Date().toLocaleString()} -`, ...args);
};
const delay = (ms: number) => new Promise(r => setTimeout(r, ms));
const fixUTC = (s: string) => {
    const a = s.split('');
    [a[5],a[6], a[8],a[9]] = [a[8],a[9], a[5],a[6]];
    return a.join('');
};
const checkDate = (s: any) => {
    if (typeof s === "string") {
        const d = new Date(fixUTC(s));
        if (!isNaN(d.getTime()))
            return d;
    }
    return null;
};
const asArray = (o: any) =>
    o instanceof Array  ? o :
    o != null           ? [o] :
                          null;
const asString = (x: any) =>
    typeof x === "string"   ? x as string :
    x != null               ? x.toString() as string :
                              null;
const asNumber = (x: any) =>
    typeof x === "number" && !isNaN(x)  ? x as number :
                                          null;
const retryOnError = async <T> (desc: string, block: () => Promise<T>) => {
    let errors = 0;
    while (true)
        try {
            return await block();
        } catch (e) {
            ++errors;
            log(`'${desc}' failed ${errors} time(s)!`);
            if (errors >= Consts.maxErrors)
                throw e;
            await delay(1000);
        }
};
const download = async <T> (url: string, encoding: string, parse: (a: string) => T) =>
    retryOnError(`Download ${url}`, async () => {
        const decoder = new TextDecoder(encoding);
        const reader = (await fetch(url)).body?.getReader();
        if (reader) {
            let text = '';
            do {
                var {done, value} = await reader.read();
                text += decoder.decode(value, {stream: !done});
            } while (!done);
            text = text.trim();
            if (text.length)
                try {
                    return parse(text);
                } catch (e) {
                    console.error('Parse error for', Deno.inspect(text, {colors: true}));
                    throw e;
                }
        }
    });
const downloadJson = (url: string, encoding: string) =>
    download(url, encoding, JSON.parse);
const downloadXml = (url: string, encoding: string) =>
    download(url, encoding, XML.parse);
const getInfo = async(date: Date, id: string): Promise<VideoResult | undefined> => {
    // Could also use `https://api-media.ccma.cat/pvideo/media.jsp?media=video&idint=${id}`,
    // but that one is used in production.
    const json =
        await downloadJson(`http://dinamics.ccma.cat/pvideo/media.jsp?media=video&idint=${id}`, Consts.tv3Encoding);
    const result = processInfoJson(json, date, id);
    if (result != null) {
        log(`Found ${id} (${result.info.seriesName} | ${result.info.title})`);
        if (!result.videos.length) {
            await getRetiredVideos(date, id, result.videos);
            log(`Found ${result.videos.length} retired videos for ${id}`);
        }
        return result;
    }
};
const getRetiredVideos = async(date: Date, id: string, videos: Video[]) => {
    const mp4Xml =
        await downloadXml(`http://www.tv3.cat/pvideo/FLV_bbd_dadesItem_MP4.jsp?idint=${id}`, Consts.tv3Encoding);
    if (processMp4Xml(mp4Xml, date, id, videos) == "hasFLV") {
        const flvXml =
            await downloadXml(`http://www.tv3.cat/pvideo/FLV_bbd_media.jsp?ID=${id}&PROFILE=EVP&FORMAT=FLV`, Consts.tv3Encoding);
        processFlvXml(flvXml, date, id, videos);
    }
};
const processInfoJson = (json: any, date: Date, id: string): VideoResult | undefined => {
    if (json && json.informacio && !json.informacio.arafem) {
        const {
            categoria_bb_ppal, programa, capitol, titol, durada,
            data_emissio, data_caducitat,
        } = json.informacio;
        const {subtitols, media, audiencies} = json;
        const info = new Info(
            date,
            id,
            asString(audiencies?.matr?.parametres?.content?.canal),
            asString(categoria_bb_ppal),
            asString(programa),
            asNumber(capitol),
            asString(titol),
            asString(durada.text),
            checkDate(data_emissio?.utc),
            checkDate(data_caducitat?.utc),
        );
        const videos: Video[] = [];
        for (const {url, format} of asArray(media) ?? [])
            if (typeof url === "string")
                videos.push(new Video(
                    date,
                    id,
                    null,
                    asString(format),
                    url,
                ));
            else for (const {file, label} of asArray(url) ?? [])
                videos.push(new Video(
                    date,
                    id,
                    asString(label),
                    asString(format),
                    asString(file),
                ));
        const subtitles: Subtitle[] = [];
        for (const {iso, format, url} of asArray(subtitols) ?? [])
            subtitles.push(new Subtitle(
                date,
                id,
                asString(iso),
                asString(format),
                asString(url),
            ));
        return {info, videos, subtitles};
    }
};
const processMp4Xml = (xml: any, date: Date, id: string, videos: Video[]) => {
    let result: "hasFLV" | undefined;
    for (const {video} of asArray(xml?.item?.videos) ?? [])
        for (const {file, qualitat, format} of asArray(video) ?? [])
            if (typeof file?.$ === 'string')
                videos.push(new Video(
                    date,
                    id,
                    asString(qualitat.$),
                    asString(format),
                    file.$,
                ));
            else if (format == 'FLV')
                result = "hasFLV";
    return result;
};
const processFlvXml = (xml: any, date: Date, id: string, videos: Video[]) => {
    for (const {$} of asArray(xml?.tv3alacarta?.item?.media) ?? [])
        if (typeof $ === 'string')
            videos.push(new Video(
                date,
                id,
                "H",
                "FLV",
                $,
            ));
};
const initDb = async (client: MySQLClient) => {
    await client.execute(`CREATE DATABASE IF NOT EXISTS ${ProgArgs.database};`);
    await client.execute(`USE ${ProgArgs.database};`);

    await client.execute(`
        CREATE TABLE IF NOT EXISTS ${Info.table} (
            date                    DATETIME NOT NULL,
            videoId                 VARCHAR(20) NOT NULL,
            channel                 VARCHAR(20),
            seriesId                VARCHAR(50),
            seriesName              VARCHAR(250),
            episode                 INT,
            title                   VARCHAR(250),
            duration                TIME,
            broadcastDate           DATETIME,
            expirationDate          DATETIME,
            UNIQUE KEY videoId (videoId)
        ) CHARACTER SET 'utf8mb4'
        COLLATE 'utf8mb4_general_ci';
    `);

    await client.execute(`
        CREATE TABLE IF NOT EXISTS ${Video.table} (
            date                    DATETIME NOT NULL,
            videoId                 VARCHAR(20) NOT NULL,
            label                   VARCHAR(20),
            format                  VARCHAR(20),
            url                     VARCHAR(100),
            v_label                 VARCHAR(20) GENERATED ALWAYS AS (COALESCE(label, "(nil)")) VIRTUAL,
            v_format                VARCHAR(20) GENERATED ALWAYS AS (COALESCE(format, "(nil)")) VIRTUAL,
            v_url                   VARCHAR(100) GENERATED ALWAYS AS (COALESCE(url, "(nil)")) VIRTUAL,
            UNIQUE KEY videoId (videoId, v_label, v_format, v_url)
        ) CHARACTER SET 'utf8mb4'
        COLLATE 'utf8mb4_general_ci';
    `);

    await client.execute(`
        CREATE TABLE IF NOT EXISTS ${Subtitle.table} (
            date                    DATETIME NOT NULL,
            videoId                 VARCHAR(20) NOT NULL,
            language                VARCHAR(20),
            format                  VARCHAR(20),
            url                     VARCHAR(100),
            v_language              VARCHAR(20) GENERATED ALWAYS AS (COALESCE(language, "(nil)")) VIRTUAL,
            v_format                VARCHAR(20) GENERATED ALWAYS AS (COALESCE(format, "(nil)")) VIRTUAL,
            v_url                   VARCHAR(100) GENERATED ALWAYS AS (COALESCE(url, "(nil)")) VIRTUAL,
            UNIQUE KEY videoId (videoId, v_language, v_format, v_url)
        ) CHARACTER SET 'utf8mb4'
        COLLATE 'utf8mb4_general_ci';
    `);
};
const calcScanRange = async (client: MySQLClient): Promise<{start: number, end: number}> => {
    const calcId = async (op: string) => {
        const {rows} = await client.execute(`SELECT ${op}(CAST(videoId AS INT)) as result FROM ${Info.table};`);
        if (rows instanceof Array && rows.length > 0 && rows[0].result != null)
            return rows[0].result;
        throw Error(`Cannot compute ${op} video ID from database`);
    };
    const calcArg = async (arg: any) => {
        if (typeof arg == 'string')
            return await calcId(arg.toUpperCase());
        return arg;
    };
    return {start: await calcArg(ProgArgs.start), end: await calcArg(ProgArgs.end)};
};
const toSqlValue = (obj: object) =>
    `(${
        Object.values(obj)
            .map(value =>
                SQLBuilder.replaceParams("?", [value]))
            .join(",")
    })`;
const sqlInsertStmt = (spec: TableSpec, objs: object[]) =>
    // Pre: objs is non-empty. The order and name of the keys in each object match
    //      the columns of the destination table.
`   INSERT INTO ${spec.table} (${spec.keys.join(",")})
    VALUES ${objs.map(toSqlValue).join(",")}
    ON DUPLICATE KEY UPDATE ${spec.nonUniqueKeys.map(k => `${k}=VALUES(${k})`).join(",")};
`;
const handleSignal = (signal: Deno.Signal, callback: () => any) => {
    if (Deno.build.os === 'windows')
        return {dispose() {}};
    const sig = Deno.signal(signal);
    sig.then(callback);
    return sig;
};

class ScanState {

    private readonly date = new Date;
    private readonly step: number;

    constructor(
        private readonly client: MySQLClient,
        private readonly start: number,
        private readonly end: number,
    ) {
        this.step = start <= end ? 1 : -1;
    }

    async *saver(): AsyncGenerator<void, void, Promise<any> | undefined> {
        let pending: Promise<VideoResult | undefined>[] = [];
        let lastFlush = Date.now();
        while (true) {
            const p = yield;
            if (p != null)
                pending.push(p);
            const now = Date.now();
            if (p == null || now - lastFlush >= Consts.saveEveryMs) {
                const results = await Promise.all(pending);
                pending = [];
                log('Storing...');
                await this.storeResults(results.filter(x => x != null) as VideoResult[]);
                lastFlush = now;
            }
        }
    }

    async createSaver() {
        const saver = this.saver();
        await saver.next();
        return saver;
    }

    async run() {
        let cancelled = false;
        const sig = handleSignal("SIGINT", () => cancelled = true);
        try {
            let id = this.start;
            const saver = await this.createSaver();
            loop:
            while (!cancelled) {
                const d = delay(1000);
                for (let j = 0; j < Consts.videosPerSecond; ++j) {
                    if (id == this.end)
                        break loop;
                    await saver.next(
                        this.processVideo(`${id}`)
                    );
                    id += this.step;
                }
                await d;
            }
            await saver.next();
        } finally {
            sig.dispose();
        }
    }

    async processVideo(id: string) {
        try {
            return await getInfo(this.date, id);
        } catch (e) {
            console.error(`Exception caught for ${id}!`);
            throw e;
        }
    }

    storeResults(results: VideoResult[]) {
        const promises = [];
        const infos = results.map(x => x.info);
        if (infos.length)
            promises.push(
                this.client.execute(
                    sqlInsertStmt(Info, infos)
                )
            );
        const videos = results.flatMap(x => x.videos)
        if (videos.length)
            promises.push(
                this.client.execute(
                    sqlInsertStmt(Video, videos)
                )
            );
        const subtitles = results.flatMap(x => x.subtitles)
        if (subtitles.length)
            promises.push(
                this.client.execute(
                    sqlInsertStmt(Subtitle, subtitles)
                )
            );
        return Promise.all(promises);
    }

};

if (ProgArgs.help != null)
    console.log(usageString);
else {
    validateArgs();
    const client = await new MySQLClient().connect({
        hostname: ProgArgs.hostname,
        port: ProgArgs.port,
        username: ProgArgs.username,
        password: ProgArgs.password,
    });
    try {
        await initDb(client);
        const {start, end} = await calcScanRange(client);
        console.log(`Scanning video ID range ${start} to ${end}`);
        await new ScanState(client, start, end).run();
    } finally {
        log('Closing...');
        await client.close();
    }
}
