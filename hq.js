const usageString =
`Usage: deno run --allow-net hq.js --domain=<domain> --timestamp=<timestamp> [OPTIONS]
Options:
  --offset=<offset>, default '0'.
  --help
    * Display this help and exit.`;

import * as Std from "https://deno.land/std/flags/mod.ts";

const Consts = {
    requestDelay: 1100,
    maxRetries: 3,
};

const DefaultArgs = {
    offset: 0,
};

const ProgArgs = Std.parse(Deno.args, {
    string: ['domain', 'timestamp', 'offset'],
    default: DefaultArgs,
});

const log = {
    lastNewLine: true,
    async println() {
        console.log(`${this.lastNewLine ? "" : '\n'}${new Date().toLocaleString()} -`, ...arguments);
        this.lastNewLine = true;
    },
    async print(text) {
        await Deno.writeAll(Deno.stdout, new TextEncoder().encode(text));
        this.lastNewLine = false;
    }
};
const delay = ms => new Promise(r => setTimeout(r, ms));
const makeUrl = (base, t) => `${base}/${t % 10}/${Math.floor((t % 100) / 10)}/${t}.mp4`;
const tryFetch = async (resource, init) => {
    for (let i = 1; i < Consts.maxRetries; ++i)
        try {
            return await fetch(resource, init);
        } catch {
            await log.println(`Got an exception, retrying...`);
            await delay(Consts.requestDelay);
        }
    return await fetch(resource, init);
}

if (ProgArgs.help != null || ProgArgs.domain == null || ProgArgs.timestamp == null) {
    console.log(usageString);
    Deno.exit(ProgArgs.help != null ? 0 : 1);
}

const invalidArg = (arg) => {
    throw Error(`Invalid value for option '${arg}': ${ProgArgs[arg]}`);
};

const {domain} = ProgArgs;
const timestamp = parseInt(ProgArgs.timestamp);
const offset = parseInt(ProgArgs.offset);

if (typeof domain != 'string') invalidArg('domain');
if (!Number.isInteger(timestamp)) invalidArg('timestamp');
if (!Number.isInteger(offset)) invalidArg('offset');

let i = offset;
while (true) {
    let d = delay(Consts.requestDelay);
    const {url, status} = await tryFetch(makeUrl(domain, timestamp + i), {method: "HEAD"});
    if (status == 403) {
        await log.println('Got a 403, retrying...');
        await delay(2*Consts.requestDelay);
    } else {
        if (status != 404)
            await log.println(`HEAD ${status} - ${url}`);
        await log.print(`\ri == ${i}`);
        await d;
        ++i;
    }
}
