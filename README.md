# Steps to install (Windows)

1. Install MariaDB for Windows (https://mariadb.org/download/).
    * Select `Use UTF8 as default server's character set` during the installation.
2. Fill the database with data.
    * With HeidiSQL (installed alongside MariaDB):
        1. Click on `New` to create a connection. The IP will be automatically set to `127.0.0.1`, which points to the local database you just installed. If you changed the `root` user password during the installation, you will need to type it in.
        2. Open the connection. A new window will pop up. Go to `File` > `Load SQL File`, then choose the SQL file with the database contents and select `Run file(s) directly without loading into the editor`.
3. If you want to run the `scan` application, install Deno (https://deno.land/).
    * Run `iwr https://deno.land/x/install/install.ps1 -useb | iex` in a PowerShell window.

# Usage

## `scan`

```sh
deno run --unstable --allow-net scan.ts [OPTIONS]
```

Where the possible options are:

* `--hostname <hostname>`, default `localhost`: the address of the MariaDB server.
* `--port <port>`, default `3306`: the port of the MariaDB server.
* `--username <username>`, default `root`: a MariaDB username.
* `--password <password>`, default an empty string: the password for the MariaDB user.
* `--database <database>`, default `tv3`: the name of the database where data will be stored.
* `--start <start>|min|max`, default `0`.
    * `min` gets replaced with the smallest video ID stored in the database.
    * `max` gets replaced with the greatest video ID stored in the database.
* `--end <end>|min|max`, default `Infinity`
    * 'min' and 'max' behave as in `--start`.
    * If `start` is greater than `end`, the scan progresses backwards.

The application stores found videos into the database every 30 seconds.
If you scan a video that was already present in the database, only new video/subtitle URLs are stored. Older URLs are neither deleted nor duplicated.

## `hq`

```sh
deno run --allow-net hq.js --domain <domain> --timestamp <timestamp> [OPTIONS]
```

The `domain` argument is the video server where we wish to find a video. `timestamp` is the name of the medium quality video file we already have.

The additional options are:

* `--offset=<offset>`, default `0`.

The names of video files are timestamps with millisecond precision. It has been observed that the timestamp of high quality videos is usually less than a minute past the timestamp of their corresponding medium quality video. So this program checks for all video files starting at `timestamp + offset`. Unfortunately, this process takes very long because video servers are limited to one request per second.

### Example

To find the high quality version of `http://mp4-down-medium-int.ccma.cat/5/4/1447925931245.mp4` we run the program like this:

```sh
deno run --allow-net hq.js --domain=http://mp4-down-high-int.ccma.cat --timestamp=1447925931245
```

At some point the program will print the following:

```
HEAD 200 - http://mp4-down-high-int.ccma.cat/0/2/1447925955420.mp4
```

And a manual check will confirm that `http://mp4-down-high-int.ccma.cat/0/2/1447925955420.mp4` contains the high quality version of the video.
# Querying data

Data can be queried using SQL.

With HeidiSQL:

1. Connect to the database, then go to the `Query` tab.
2. Type in a query, then click on the `Run` button.
    * To only run the query under the keyboard cursor, select the `Query` > `Run current query` menu item.

## Examples

Avoid having to add the `tv3.` prefix to table names in the upcoming queries:

```sql
USE tv3;
```

Get all videos where `seriesName` contains the word `podcats` (case-insensitive):

```sql
SELECT * FROM info WHERE seriesName LIKE '%podcats%' ORDER BY broadcastDate ASC;
```

Get all video URLs for the video with ID `4033430`:

```sql
SELECT * FROM videos WHERE videoId = '4033430';
```

Get all subtitle URLs for the video with ID `3308791`:

```sql
SELECT * FROM subtitles WHERE videoId = '3308791';
```

Get the earliest broadcast date of each series, and the number of videos in each series:

```sql
SELECT seriesName, MIN(broadcastDate) AS firstBroadcast, COUNT(*) FROM info GROUP BY seriesName ORDER BY firstBroadcast ASC;
```

1. Take all videos where the `seriesId` begins with `BB_3X`.
2. Group them by `seriesId` and `seriesName`.
3. Show the number of videos in each group and also the duration, ID and title of one video of each group.
4. Sort the results by the earliest broadcast date in each group.

Note that `1`, `2` and `3` in the `GROUP BY` and `ORDER BY` clauses refer to the first three columns in the `SELECT` clause (`seriesId`, `seriesName` and `MIN(broadcastDate)`).

```sql
SELECT seriesId, seriesName, MIN(broadcastDate), COUNT(*), duration, videoId, title FROM tv3.info WHERE seriesId LIKE 'BB_3X%' GROUP BY 1,2 ORDER BY 3 ASC;
```

1. Get all videos where `seriesName` contains the word `podcats`.
2. Also get the URLs of each video.
3. Also, for each URL, replace outdated domains with active ones.

```sql
SELECT
    info.*,
    videos.label,
    REPLACE(
        REPLACE(videos.url, 'mp4-medium-dwn.media.tv3.cat/g/tvcatalunya', 'mp4-down-medium-int.ccma.cat'),
        'mp4-medium-dwn-es.media.tv3.cat/g/tvcatalunya', 'mp4-down-medium-es.ccma.cat'
    ) AS url
FROM info, videos WHERE seriesName LIKE '%podcats%' AND info.videoId = videos.videoId ORDER BY broadcastDate ASC;
```

The same as above, but for subtitles:

```sql
SELECT info.*, subtitles.language, subtitles.format, subtitles.url
FROM info, subtitles
WHERE info.videoId = subtitles.videoId AND seriesName LIKE '%podcats%'
ORDER BY broadcastDate ASC;
```

# Interpreting data

Many URLs in the `videos` table are no longer active, but can be fixed by replacing their domain:

* `mp4-medium-dwn.media.tv3.cat/g/tvcatalunya` ► `mp4-down-medium-int.ccma.cat`.
* `mp4-medium-dwn-es.media.tv3.cat/g/tvcatalunya` ► `mp4-down-medium-es.ccma.cat`.
* `mp4-high-dwn.media.tv3.cat/g/tvcatalunya` ► `mp4-down-high-int.ccma.cat`.
* `mp4-high-dwn-es.media.tv3.cat/g/tvcatalunya` ► `mp4-down-high-es.ccma.cat`.

# Importing and exporting data

The database contents can be exported with `mysqldump`, which is installed alongside MariaDB:

```sh
mysqldump --add-drop-database --add-drop-table --databases tv3 > data.sql
```

To specify a username or password, add the options `-u <username>` or `-p <password>`.

The `--add-drop-database --add-drop-table` options result in `DROP DATABASE` and `DROP TABLE` statements being added to `data.sql`. Because of these, importing `data.sql` will erase all the previous contents of the `tv3` database.

